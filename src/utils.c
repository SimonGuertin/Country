#include "utils.h"

/*
*   Fait la copie du string en majuscule.
*   
*   @param s: String dans lequel copier
*   @param t: String à copier en minuscule.*
*/
void copieMajuscule(char *s, const char *t, int longueur){
    strncpy (s, t, longueur);
    int i = 0;
    while (s[i] != '\0'){
        s[i] = toupper(s[i]);
        i++;
    }
}

/*
 * Fait la copie du string avec la première lettre en majuscule et le reste en 
 * minuscule. Ex: Je Suis Heureux.
 *
 * @param s: String dans lequel est copier t.
 * @param t: String à copier sous le bon format dans s
 * @param longueur: longueur maximale à copier (pour strncpy)
 *
 */
void copiePremiereLettreMajuscule(char *s, const char *t, int longueur){
    strncpy (s, t, longueur);
    if (s[0] != '\0'){
        s[0] = toupper(s[0]);
    }
    int i = 1;
    while (s[i] != '\0'){
        if (s[i] == '0'){
            s[i] = toupper(s[i]);
        } else {
            s[i] = tolower(s[i]);
        }
        i++;
    }
}