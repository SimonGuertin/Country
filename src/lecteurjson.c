#include "lecteurjson.h"
#include <jansson.h>
#include "parsearg.h"
#include <string.h>

void afficherCapitale(json_t* pays);
void afficherLangages(json_t* pays);
void afficherBorders(json_t* pays);

void lireJson(struct Arguments arguments){
    json_error_t erreur;
    json_t* racine = json_load_file("data/countries.json", 0, &erreur);

    if (!racine || racine->type != JSON_ARRAY){
    	printf(ERREUR_JSON);
    	return;
    }

    size_t tailleJson = json_array_size(racine);
    struct Langues langues[MAX_PARAM_LANGUES];
    bool sameBordersOk = true;

    for (int i = 0; i < 3; i++){
        langues[i].nbLangues = 0;
    }

    for (size_t i = 0; i < tailleJson ; i++){
    	json_t* pays = json_array_get(racine, i);
    	if (arguments.country || arguments.allCountries){    		
			printCountry(arguments, pays);
        } else if (arguments.region){
            printRegion(arguments, pays); 
    	} else if (arguments.sameLanguage){
    		initialiserStructLangues(arguments, pays, langues);
    	} else if (arguments.sameBorders){
            int bordersCommunes = verifierSiMemeBorders(arguments, pays);
            if (bordersCommunes != -1 && bordersCommunes != arguments.nbPays-1){
                sameBordersOk = false;            
            }
    	} else {
    		printf("Erreur: Option non reconnue");
    		return;
    	}    	
    }

    if(arguments.sameLanguage){
        reponseSameLanguage(arguments, langues);
    }
    if(arguments.sameBorders){
        if (sameBordersOk){
            printf("yes");
        } else {
            printf("no");
        }
    }

    return;
}

void printCountry(struct Arguments arguments, json_t* pays){

	json_t* codePays = json_object_get(pays, CODE_PAYS);

    if (!codePays) return;
    if (codePays->type != JSON_STRING){
    	printf(ERREUR_JSON);
    	return;
    }

    const char* code = json_string_value(codePays);    
    if (!strcmp(code, arguments.code) || arguments.allCountries){
    	json_t* objetNom = json_object_get(pays, SECTION_NOM_PAYS);
    	json_t* objetCommon = json_object_get(objetNom, NOM_PAYS);
    	const char* nomPays = json_string_value(objetCommon);

    	printf("Name: %s\n", nomPays);
    	printf("Code: %s\n", code);

        for (int i = 1; i < 4; i++){
    	    if(arguments.showCapital && arguments.prioCapitale == i){
    		    afficherCapitale(pays);
    	    }
        	if(arguments.showLanguage && arguments.prioLangue == i){
    	    	afficherLangages(pays);
        	}
    	    if (arguments.showBorders && arguments.prioBorder == i){
    		    afficherBorders(pays);    		
            }
        }
    }
    return;
}

void printRegion(struct Arguments arguments, json_t* pays){

	json_t* objetRegion = json_object_get(pays, REGION_PAYS);

    if (!objetRegion) return;
    if (objetRegion->type != JSON_STRING){
    	printf(ERREUR_JSON);
    	return;
    }

    const char* region = json_string_value(objetRegion);    
    if (!strcmp(region, arguments.nomRegion)){
    	json_t* objetNom = json_object_get(pays, SECTION_NOM_PAYS);
    	json_t* objetCommon = json_object_get(objetNom, NOM_PAYS);
    	const char* nomPays = json_string_value(objetCommon);

        json_t* codePays = json_object_get(pays, CODE_PAYS);
        const char* code = json_string_value(codePays);

    	printf("Name: %s\n", nomPays);
    	printf("Code: %s\n", code);
        for (int i = 1; i < 4 ; i++){
    	    if(arguments.showCapital && arguments.prioCapitale ==i){
    	    	afficherCapitale(pays);
    	    }
    	    if(arguments.showLanguage && arguments.prioLangue == i){
    	    	afficherLangages(pays);
    	    }
    	    if (arguments.showBorders && arguments.prioBorder == i){
    		    afficherBorders(pays);    		
		    }	
        }
    }
    return;

}

void afficherCapitale(json_t* pays){
	json_t* objetCapitale = json_object_get(pays, CAPITALE_PAYS);
	size_t nombreCapitales = json_array_size(objetCapitale);
	printf("Capital: ");
	for (size_t i = 0; i < nombreCapitales; i++){
		json_t* tableauCapitales = json_array_get(objetCapitale, i);
		const char* capitale = json_string_value(tableauCapitales);
		printf("%s", capitale);
		if (i < nombreCapitales-1){
			printf(", ");
		}
	}
	printf("\n");
}

void afficherLangages(json_t* pays){
	json_t* objetLangues = json_object_get(pays, SECTION_LANGUES);

	size_t nombreLangues = json_object_size(objetLangues);	
	printf("Languages: ");
    
    const char *key;
    json_t *value;
    int i = 0;
    json_object_foreach(objetLangues, key, value){
        printf("%s", json_string_value(value));
        if (i < nombreLangues-1){
			printf(", ");
		}
        i++;
	}
	printf("\n");
}

void afficherBorders(json_t* pays){
	json_t* objetBorder = json_object_get(pays, BORDER_PAYS);
	
	size_t nombreBorders = json_array_size(objetBorder);
	printf("Borders: ");
	for (size_t i = 0; i < nombreBorders; i++){
		json_t* tableauBorders = json_array_get(objetBorder, i);
		const char* border = json_string_value(tableauBorders);
		printf("%s", border);
		if (i < nombreBorders-1){
			printf(", ");
		}
	}
	printf("\n");	
}

void initialiserStructLangues(struct Arguments arguments, json_t* pays,
                        struct Langues langues[MAX_PARAM_LANGUES]){

    json_t* objetCode = json_object_get(pays, CODE_PAYS);

    const char* code = json_string_value(objetCode);
    for (int i = 0; i < arguments.nbPays; i++){
        if(!strcmp(arguments.pays[i], code)){
            json_t* objetLangues = json_object_get(pays, SECTION_LANGUES);
            const char *key;
            json_t *value;
            json_object_foreach(objetLangues, key, value){
                strcpy(langues[i].nomLangues[langues[i].nbLangues], json_string_value(value));
                langues[i].nbLangues++;
            }       
        }
    }
	return;
}

void reponseSameLanguage(struct Arguments arguments, struct Langues langues[MAX_PARAM_LANGUES]){
    bool memeLangues = false;
    
    struct Langues langCommune;
    langCommune.nbLangues = 0;

    for (int i = 0; i < langues[0].nbLangues; i++){
        for (int j = 0; j < langues[1].nbLangues; j++){
            if (!strcmp(langues[0].nomLangues[i], langues[1].nomLangues[j])){
                if (arguments.nbPays == 3){
                    for (int k = 0; k < langues[2].nbLangues; k++){
                        if(!strcmp(langues[0].nomLangues[i], langues[2].nomLangues[k])){
                            memeLangues = true;
                            strcpy(langCommune.nomLangues[langCommune.nbLangues], 
                                    langues[0].nomLangues[i]);
                            langCommune.nbLangues++;
                        }       
                    }
                } else {
                    memeLangues = true;
                    strcpy(langCommune.nomLangues[langCommune.nbLangues], 
                              langues[0].nomLangues[i]);
                    langCommune.nbLangues++;

                }
            }
        }                   
    }

    if (memeLangues){
        printf("yes ");
        for(int i = 0; i < langCommune.nbLangues; i++){
            printf("%s ", langCommune.nomLangues[i]);
        }
    } else {
        printf("no");
    }
    return;
}

int verifierSiMemeBorders(struct Arguments arguments, json_t* pays){
    json_t* objetCode = json_object_get(pays, CODE_PAYS);
    const char* code = json_string_value(objetCode);
    bool paysRecherche = false;
    int bordersCommunes = 0;
    for (int i = 0; i < arguments.nbPays; i++){
        if (!strcmp(code, arguments.pays[i])){
            paysRecherche = true;
        }
    }

    if (paysRecherche){
        json_t* objetBorder = json_object_get(pays, BORDER_PAYS);
        size_t nombreBorders = json_array_size(objetBorder);

        for(size_t i = 0; i < nombreBorders; i++){
            json_t* tableauBorder = json_array_get(objetBorder, i);
            const char* border = json_string_value(tableauBorder);
            for (int j = 0; j < arguments.nbPays; j++){
                if (!strcmp(border, arguments.pays[j])){
                    bordersCommunes++;
                }
            }
        }
    } else {
        return -1;        
    }
	return bordersCommunes;
}



