/**
 * TP3 - Construction et maintenance de logiciels
 *
 * Auteur:          Simon Guertin
 * Code Permanent:  GUES05018704
 *
 */

#include <stdio.h>
#include <jansson.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "parsearg.h"
#include "lecteurjson.h"

int main (int argc, char *argv[]){
    struct Arguments arguments = lireArguments(argc, argv);
    if (arguments.status != TP_OK){
        return arguments.status;
    }
    lireJson(arguments);    

    return 0;
}
