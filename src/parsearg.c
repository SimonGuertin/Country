#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <ctype.h>
#include "parsearg.h"
#include "utils.h"

struct Arguments lireArguments(int argc, char *argv[]){
    struct Arguments arguments;

    //Arguments par défaut
    arguments.allCountries = true;
    arguments.country = false;
    arguments.showCapital = false;
    arguments.showLanguage = false;
    arguments.showBorders = false;
    arguments.region = false;
    arguments.sameLanguage = false;
    arguments.sameBorders = false;
    arguments.status = TP_OK;
    //Priorité d'affichage des options --show-langaguges, --show-borders, --show-capital
    int priorite = 1;

    struct option longOpts[] = {
        {"country",         required_argument, 0, 'c'},
        {"show-capital",    no_argument,       0, 's'},
        {"show-borders",    no_argument,       0, 'b'},
        {"show-languages",  no_argument,       0, 'l'},
        {"region",          required_argument, 0, 'r'},
        {"same-borders",    required_argument, 0, 'o'},
        {"same-language",   required_argument, 0, 'a'},
        {0,0,0,0},
    };

    // Remettre optind des arguments à zéro pour le fonctionnement des tests CUnit.
    optind = 0;

    int c = getopt_long(argc, argv, "c:sblr:o:a:", longOpts, &optind);

    while(c != -1){
        switch (c) {
            case 'c': arguments.country = true;
                      arguments.allCountries = false;
                      copieMajuscule(arguments.code, optarg, LONGUEUR_CODE);
                      break;
            case 's': arguments.showCapital = true;
                      arguments.prioCapitale = priorite;
                      priorite++;
                      break;
            case 'b': arguments.showBorders = true;
                      arguments.prioBorder = priorite;
                      priorite++;
                      break;
            case 'l': arguments.showLanguage = true;
                      arguments.prioLangue = priorite;
                      priorite++;
                      break;
            case 'r': arguments.region = true;
                      arguments.allCountries = false;
                      copiePremiereLettreMajuscule(arguments.nomRegion, optarg, LONGUEUR_REGION);
                      break;
            case 'o': arguments.allCountries = false;
                      if (argc < 3 || argc > 5){
                          arguments.status = TP_NB_ARG_INVALIDE;
                          printf(MSG_NB_ARG_INVALIDE);
                      } else if (!strcmp(argv[2], "--same-borders")){
                          arguments.status = TP_ERREUR_MODE;
                          printf(MSG_ERREUR_MODE);
                      } else {                  
                         arguments.sameBorders = true;
                         arguments.nbPays = 0;
                         for (int i = 2; i < argc; i++){
                                copieMajuscule(arguments.pays[i-2], argv[i], LONGUEUR_CODE);
                                arguments.nbPays++;
                         }
                      }                      
                      break;
            case 'a': arguments.allCountries = false;
                      if (argc < 3 || argc > 5){
                          arguments.status = TP_NB_ARG_INVALIDE;
                          printf(MSG_NB_ARG_INVALIDE);
                      } else if (!strcmp(argv[2], "--same-language")){
                          arguments.status = TP_ERREUR_MODE;
                          printf(MSG_ERREUR_MODE);
                      } else {
                          arguments.sameLanguage = true;
                          arguments.nbPays = 0;
                          for (int i = 2; i < argc; i++){
                                copieMajuscule(arguments.pays[i-2], argv[i], LONGUEUR_CODE);
                                arguments.nbPays++;
                          }                          
                      }
                      break;
            case '?': arguments.status = TP_MAUVAISE_OPTION;
                      break;
        }
        c = getopt_long(argc, argv, "c:sblr:o:a:", longOpts, &optind);
    }
    int nbModes = 0;
    if (arguments.country) nbModes++;
    if (arguments.region) nbModes++;
    if (arguments.sameLanguage) nbModes++;
    if (arguments.sameBorders) nbModes++;
    if (arguments.allCountries) nbModes++;
    if (nbModes != 1){
        printf(MSG_ERREUR_MODE);
        arguments.status = TP_ERREUR_MODE;
    }
    if (arguments.status == TP_MAUVAISE_OPTION){
        printf("Erreur: Une option n'as pas été reconnue");
    }   

    return arguments;
}




