/**
 * utils.h
 *
 * Fonctions utilitaires.
 *
 * Autheur: Simon Guertin
 */

#ifndef PARSEARG_H_
#define PARSEARG_H_

#include <string.h>
#include <ctype.h>
#include "parsearg.h"

/*
*   Fait la copie du string en majuscule.
*   
*   @param s: String dans lequel copier
*   @param t: String à copier en minuscule.*
*/
void copieMajuscule(char *s, const char *t, int longueur);


/*
 * Fait la copie du texte en format pascal: Première lettre de chaque mot
 * en majuscule et le reste en minuscule.
 *
 * @param s: String dans lequel copier
 * @param t: String original
 * @param longueur: longueur maximale de la copie
 */
void copiePremiereLettreMajuscule(char *s, const char *t, int longueur);

#endif