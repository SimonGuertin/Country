#include <stdio.h>
#include <string.h>
#include "CUnit/Basic.h"
#include "lecteurjson.h"
#include <jansson.h>
#include <stdbool.h>
#include "utils.h"


int init_suite1(void){
    
    return 0;
}

int clean_suite1(void){

    return 0;
}

void testOrdreOptions(void){
    char *argv[] = {"bin/tp3","--country","CAN","--show-borders","--show-languages","--show-capital"};
    char argc = 6;

    struct Arguments arguments = lireArguments(argc, argv);

    CU_ASSERT(arguments.prioBorder == 1 && "Prio de frontière == 1");
    CU_ASSERT(arguments.prioLangue == 2 && "Prio de langue == 2");
    CU_ASSERT(arguments.prioCapitale == 3 && "Prio de capitale == 3");


    //Autre ordre d'appel des options --show
    char *argv2[] = {"bin/tp3","--country","CAN","--show-capital","--show-borders","--show-languages"};
    int argc2 = 6;

    struct Arguments arguments2 = lireArguments(argc2, argv2);

    CU_ASSERT(arguments2.prioBorder == 2 && "Prio de frontière == 2");
    CU_ASSERT(arguments2.prioLangue == 3 && "Prio de langue == 3");
    CU_ASSERT(arguments2.prioCapitale == 1 && "Prio de capitale == 1");

}

void testCopiePremiereLettreMajuscule(void){
    char test[5] = "allo";
    char resultatDuTest[5];

    copiePremiereLettreMajuscule(resultatDuTest, test, 5);
    CU_ASSERT(!strcmp(resultatDuTest, "Allo") && "Vérificattion que resultat == Allo");
}

void testCopieMajuscule(void){
    char test[5] = "allo";
    char resultatDuTest[5];

    copieMajuscule(resultatDuTest, test, 5);
    CU_ASSERT(!strcmp(resultatDuTest, "ALLO"));
}

void testArgumentsCountry(void){    
    int argc = 3;
    char *argv[] = {"bin/tp3","--country","can"};
    struct Arguments arguments = lireArguments(argc, argv);

    CU_ASSERT(arguments.country && "Test de vérité de l'argument country");
    CU_ASSERT(!arguments.allCountries && "Test de fausseté de l'argument allCountries");
    CU_ASSERT(!strcmp(arguments.code, "CAN") && "Vérification du code can");
}

void testArgumentsRegion(void){    
    int argc = 3;
    char *argv[] = {"bin/tp3","--region","oceania"};
    struct Arguments arguments = lireArguments(argc, argv);

    CU_ASSERT(arguments.region && "Test de vérité de l'argument region");
    CU_ASSERT(!strcmp(arguments.nomRegion, "Oceania") && "Test du nom de la region = Oceania");
}

void testArgumentsSameLanguage(void){    
    int argc = 4;
    char *argv[] = {"bin/tp3","--same-language","can", "usa"};
    struct Arguments arguments = lireArguments(argc, argv);

    CU_ASSERT(arguments.sameLanguage && "Test de vérité de l'argument sameLanguage");
    CU_ASSERT(!strcmp(arguments.pays[0], "CAN") && "Test code de pays == CAN");
    CU_ASSERT(!strcmp(arguments.pays[1], "USA") && "Test code de pays == USA");
}

void testArgumentsSameBorders(void){    
    int argc = 4;
    char *argv[] = {"bin/tp3","--same-borders","can", "usa"};
    struct Arguments arguments = lireArguments(argc, argv);

    CU_ASSERT(arguments.sameBorders && "Test de vérité de l'argument same-borders");
    CU_ASSERT(!strcmp(arguments.pays[0], "CAN") && "Test code de pays == CAN");
    CU_ASSERT(!strcmp(arguments.pays[1], "USA") && "Test code de pays == USA");
}
    
void testPrintSameBorders(void){
    json_error_t erreur;
    json_t* racine = json_load_file("data/countries.json", 0, &erreur);
    json_t* pays = json_array_get(racine, 1);
    
    struct Arguments arguments;
    arguments.nbPays = 2;
    strcpy(arguments.pays[0], "AFG");
    strcpy(arguments.pays[1], "IRN");    

    int bordersCommunes = verifierSiMemeBorders(arguments, pays);
    CU_ASSERT(1 == bordersCommunes && "Test Border AFG IRN valide");

    strcpy(arguments.pays[1], "FRA");
    bordersCommunes = verifierSiMemeBorders(arguments, pays);
    CU_ASSERT(0 == bordersCommunes && "Test Border AFG FRA invalide");

    arguments.nbPays = 3;
    strcpy(arguments.pays[2], "PAK");
    strcpy(arguments.pays[1], "IRN");
    bordersCommunes = verifierSiMemeBorders(arguments, pays);
    CU_ASSERT(2 == bordersCommunes && "Test Border avec 3 pays");


    pays = json_array_get(racine, 200);
    bordersCommunes = verifierSiMemeBorders(arguments, pays);
    CU_ASSERT(-1 == bordersCommunes && "Test Border pays non recherché");
}

int main(){
    CU_pSuite pSuite = NULL;

    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if ((NULL == CU_add_test(pSuite, "Test verifierSiMemeBorders", testPrintSameBorders)) ||
        (NULL == CU_add_test(pSuite, "Test --country", testArgumentsCountry)) ||
        (NULL == CU_add_test(pSuite, "Test --same-language", testArgumentsSameLanguage)) ||
        (NULL == CU_add_test(pSuite, "Test --same-borders", testArgumentsSameBorders)) ||
        (NULL == CU_add_test(pSuite, "Test copiePremiereLettreMajuscule", testCopiePremiereLettreMajuscule)) ||
        (NULL == CU_add_test(pSuite, "Test copieMajuscule", testCopieMajuscule)) ||
        (NULL == CU_add_test(pSuite, "Test ordre d'options", testOrdreOptions)) ||
        (NULL == CU_add_test(pSuite, "Test --region", testArgumentsRegion))){
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

