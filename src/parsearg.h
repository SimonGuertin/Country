/**
 * lirearguments.h
 *
 * Vérifie chacun des arguments passé au programme tp3.c et
 * retourne la struct Arguments qui inclus les erreurs potentielles.
 *
 * Autheur: Simon Guertin
 */
#ifndef PARSEARG_H
#define PARSEARG_H

#include <stdbool.h>

#define LONGUEUR_CODE 5
#define NB_PAYS 3
#define LONGUEUR_REGION 80

#define MSG_ERREUR_MODE "Erreur: Il faut un seul mode qui est premier argument entre --country, --same-language, --region ou --same-borders."
#define MSG_NB_ARG_INVALIDE "Erreur: Ce mode supporte seulement 2 à 3 pays"
                            

enum Erreur {
    TP_OK              = 0,
    TP_TROP_DOPTIONS   = 1,
    TP_MAUVAISE_OPTION = 2,
    TP_ERREUR_MODE     = 3,
    TP_NB_ARG_INVALIDE = 4,
};

struct Arguments{
    bool allCountries;                  // Mode affichage de tous les pays.
    bool country;                       // Mode Information de pays.
    bool showCapital;                   // Option afficher capitale.
    int prioCapitale;                   // Priorité d'affichage    
    bool showLanguage;                  // Option afficher langues.
    int prioLangue;                     // Priorité d'affichage    
    bool showBorders;                   // Option afficher pays voisins.
    int prioBorder;                     // Priorité d'affichage    
    bool region;                        // Mode afficher les pays d'une région.
    bool sameLanguage;                  // Mode vérification de langue commune.
    bool sameBorders;                   // Mode vérifications de pays voisins.
    char code[LONGUEUR_CODE];           // Code du pays pour mode country.
    char pays[NB_PAYS][LONGUEUR_CODE];  // Pays passés en arguments.
    int nbPays;                         // Nombre de pays passé dans same-language/border.
    char nomRegion[LONGUEUR_REGION];    // Nom de la region passé en argument.
    enum Erreur status;                 // Code d'erreur s'il y a lieu.
};

/**
 * Lis chacun des arguments et les affectes à la structure 
 * Argument
 *
 * @param argc: Nombre d'arguments
 * @param argv: Les arguments
 */
struct Arguments lireArguments(int argc, char *argv[]);




#endif
