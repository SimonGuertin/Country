/*
 * lecteurjson.h
 *
 * Lis l'input de format Jansson et affiche le contenu selon les arguments 
 * passés en paramêtre.
 *
 * Auteur: Simon Guertin
 */

#ifndef LIREJSON_H
#define LIREJSON_H

#include "parsearg.h"
#include "jansson.h"

#define ERREUR_JSON "Erreur: le fichier json est mal formaté."
#define CODE_PAYS "cca3"
#define SECTION_NOM_PAYS "name"
#define NOM_PAYS "common"
#define CAPITALE_PAYS "capital"
#define BORDER_PAYS "borders"
#define SECTION_LANGUES "languages"
#define REGION_PAYS "region"

#define NB_LANG_MAX 17
#define LONGUEUR_LANG_MAX 100
#define MAX_PARAM_LANGUES 3

struct Langues { 
    char nomLangues[NB_LANG_MAX][LONGUEUR_LANG_MAX]; // Langues parlées dans ce pays.
    int nbLangues;                                   // Nombres de langues parlées dans ce pays.
};


/*
 * Initialise la lecture de l'input Jansson. et 
 * lance l'affichage demandé.
 *
 * @param arguments: Liste des arguments sélectionnés.
 */
void lireJson(struct Arguments arguments);

/**
 * Fait l'affichage selon le mode --country et ses options choisies.
 *
 * @param argument: Les arguments en paramètre.
 * @param pays: Un des pays du document json.
 */
void printCountry(struct Arguments arguments, json_t* pays);

/**
 * Lorsque le mode --same-language est appelé, charge toutes les langues
 * dans la struct Langues et le nombre de langues parlées.
 *
 * @param argument: Les arguments en paramètre.
 * @param pays: Un des pays du document json.
 * @param langues: Tableau de langues parlées
 */
void initialiserStructLangues(struct Arguments arguments, json_t* pays, 
                            struct Langues langues[MAX_PARAM_LANGUES]);

/**
 * avec le monde --same-borders, vérifie si les frontières sont communes
 *
 * @param argument: Les arguments en paramètre.
 * @param pays: Un des pays du document json.
 *
 * @return: -1 si aucune frontière commune, autrement retourne le
 *          nombre de frontières communes détectées
 */
int verifierSiMemeBorders(struct Arguments arguments, json_t* pays);

/**
 * Fait l'affichage selon le mode --region et ses options choisies
 * @param argument: Les arguments en paramètre.
 * @param pays: Un des pays du document json.
 *
 * @return: Le nombre de borders communes.
 *          -1 si le pays n'est pas un pays à vérifier.
 */
void printRegion(struct Arguments argument, json_t* pays);

/**
 * Fait l'affichage yes/no de --same-language avec l'affichage
 * des langues communes.
 *
 * @param arguments: Les arguments du programme 
 * @param langues: Tableau des langues des pays en paramètre avec
 *                 le nombre de pays
 */
void reponseSameLanguage(struct Arguments argument, 
                            struct Langues langues[MAX_PARAM_LANGUES]);

#endif
