# Projet Country

## Description

Ce projet consiste à un programme en ligne de commande qui donne de l'information sur tous les pays du monde.

Projet réalisé à l'Université de Québec à Montréal, dans le cours Construction et Maintenance de Logiciels (INF3135) à titre de travail de session.

## Auteurs

Simon Guertin

## Fonctionnement

Pour faire fonctionner le programme, il faut écrire avec la commande bin/tp3 dans le terminal lorsqu'on est dans le répertoire principal. On peut apeller le programme seul, ce qui donne l'information sur tous les pays du monde, ou avec l'une des 4 commandes de base:
- --country COUNTRY
- --region REGION
- --same-languages PAYS1 PAYS2 PAYS3
- --same-borders PAYS1 PAYS2 PAYS3

### Explications des commandes

#### - Sans commande

Lorsque que le programme est appelé seulement par bin/tp3, tous les pays seront affiché avec leur code et leur nom de pays.

Il existe 3 options supplémentaires pour avoir plus d'information.

- --show-capital
- --show-languages
- --show-borders

Chaque option effectue son rôle: --show-capital donne la ou les capitales du Pays, et ainsi de suite pour les 2 autres commandes.


~~~
 $ bin/tp3 --show-borders --show-languages --show-capital
Name: Aruba
Code: ABW
Borders:
Languages: Dutch, Papiamento
Capital: Oranjestad
Name: Afghanistan
Code: AFG
Borders: IRN, PAK, TKM, UZB, TJK, CHN
Languages: Dari, Pashto, Turkmen
Capital: Kabul
Name: Angola
...
~~~

#### - Commande --country COUNTRY


Pour utiliser la fonction country, vous devez remplacer "COUNTRY" par un code de pays à 3 lettres. Exemple: le code du Canada est CAN, les États-Unis est USA. La commande de base donne le nom du pays et son code qui lui est attaché.

Comme le programme de base, on peut ajouté les option --show-capital --show-languages et --showborders

~~~
$ bin/tp3 --country can --show-capital --show-languages --show-borders
Name: Canada
Code: CAN
Capital: Ottawa
Languages: English, French
Borders: USA
~~~

#### - Commande --region REGION
Cette commande fonctionne exactement comme la commande --country, sauf qu'il faut lui donner le nom de la région désirée (asia, oceania, americas, africa, europe). Les mêmes options que pour --country s'applique. 

Voici un exemple d'affichage (Écourté)
~~~
$ bin/tp3 --region oceania --show-capital --show-languages --show-borders

Name: American Samoa
Code: ASM
Capital: Pago Pago
Languages: English, Samoan
Borders:
Name: Australia
Code: AUS
Capital: Canberra
Languages: English
Borders:
...
Name: Samoa
Code: WSM
Capital: Apia
Languages: English, Samoan
Borders:
~~~

#### - Commande --same-language 
Cette commande nous laisse savoir si 2 à 3 pays ont au moins une langue commune.

Pour s'utiliser, il faut écrire --same-language suivit de 2 ou 3 codes de pays séparés par des espaces.
~~~
$ bin/tp3 --same-language can usa aus
yes English
~~~

#### - Commande --same-borders
Cette commande nous laisse savoir si 2 à 3 pays ont les mêmes frontières.

Il faut écrire --same-borders suivit de 2 ou 3 codes de pays séparés par des espaces.
~~~
$ bin/tp3 --same-borders can usa mex
no
$ bin/tp3 --same-borders fra ita che
yes
~~~

## Exécution des tests

Pour faire fonctionner la série de tests CUnit, il suffit d'écire "make test" dans le terminal

## Plateformes supportées

Ce programme a été testé sur Linux seulement.

## Dépendances

jansson: http://www.digip.org/jansson/
CUnit: http://cunit.sourceforge.net/

## Compilation

Pour compiler ce programme, il suffit de se retrouver dans le dossier principal, d'utiliser un terminal bash et d'écrire "make". Il est possible de compiler seulement les tests avec la commande "make test". La commande "make clean" efface tous les programmes créés et les dossiers bin/ et test/

## Références

Le document countries.json provient du projet [countries](https://github.com/mledoze/countries)