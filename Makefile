CC      = gcc
CFLAGS  = -Wall -c -std=c11
LFLAGS  = -ljansson
TFLAGS	= -lcunit -ljansson
EXEC 	= tp3
CFILES  = src/tp3.c src/parsearg.c src/lecteurjson.c src/utils.c
TFILES = src/test.c src/parsearg.c src/lecteurjson.c src/utils.c
OBJECTS = $(CFILES:.c=.o)
TOBJECTS = $(TFILES:.c=.o)
EXEC = bin/tp3
TEST = test/tests

all: $(EXEC) $(TEST)
	
test: $(TEST)
	test/tests

$(TEST): $(TFILES:.c=.o)
		mkdir -p test
		$(CC) -o $@ $^ $(TFLAGS)

$(EXEC): $(CFILES:.c=.o)
		mkdir -p bin
		$(CC) -o $@ $^ $(LFLAGS)

parsearg.o: parsearg.h

lecteurjson.o: lecteurjson.h

bin/%.o: %.c
		$(CC) -o $@ $(CFLAGS) $<

clean:
		rm -f $(CFILES:.c=.o)
		rm -rf bin/
		rm -rf test/
